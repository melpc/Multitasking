package com.pc.melanypatino.multitasking;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ModificarActivity extends AppCompatActivity {

    EditText editTextCodigo,  editTextPlaza;
    Button buttonModificar2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar);

        editTextCodigo = (EditText) findViewById(R.id.editTextCodigo1);
        editTextPlaza = (EditText) findViewById(R.id.editTextPlaza1);


        buttonModificar2 = (Button) findViewById(R.id.buttonModificar2);
        buttonModificar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Modificar(v);
            }
        });

    }

    public void Modificar (View view){

        DBHelper admin = new DBHelper(this);
        SQLiteDatabase db = admin.getWritableDatabase();

        String codigo = editTextCodigo.getText().toString();
        String plaza = editTextPlaza.getText().toString();

        if (!codigo.isEmpty() && !plaza.isEmpty()){
            ContentValues registro = new ContentValues();
            registro.put(DBManager.ID, codigo);
            registro.put(DBManager.PLAZA, plaza);

            int cantidad = db.update(DBManager.TABLE_NAME, registro, "codigo="+codigo, null);
            db.close();

            if (cantidad == 1) {
                Toast.makeText(this, "Codigo modificado exitosamente", Toast.LENGTH_LONG).show();
                editTextCodigo.setText("");
                editTextPlaza.setText("");

            }else{
                Toast.makeText(this, "El codigo no existe", Toast.LENGTH_LONG).show();
            }

        }else{
            Toast.makeText(this, "Debe llenar todos los campos", Toast.LENGTH_LONG).show();
        }
    }

}
