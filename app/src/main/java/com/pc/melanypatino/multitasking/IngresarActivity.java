package com.pc.melanypatino.multitasking;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class IngresarActivity extends AppCompatActivity {

    EditText editTextCodigo, editTextPlaca, editTextPlaza, editTextFecha, editTextHora;
    Button buttonIngresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar);

        editTextCodigo = (EditText) findViewById(R.id.editTextCodigo);
        editTextPlaca = (EditText) findViewById(R.id.editTextPlaca);
        editTextPlaza = (EditText) findViewById(R.id.editTextPlaza);
        editTextFecha = (EditText) findViewById(R.id.editTextFecha);
        editTextHora = (EditText) findViewById(R.id.editTextHora);

        buttonIngresar = (Button) findViewById(R.id.buttonIngresar);
        buttonIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Registrar(v);
            }
        });

    }

    public void Registrar(View view){

        DBHelper admin = new DBHelper(this);
        SQLiteDatabase db = admin.getWritableDatabase();

        String codigo = editTextCodigo.getText().toString();
        String placa = editTextPlaca.getText().toString();
        String plaza = editTextPlaza.getText().toString();
        String fecha = editTextFecha.getText().toString();
        String hora = editTextHora.getText().toString();

        if (!codigo.isEmpty() && !placa.isEmpty() && !plaza.isEmpty() && !fecha.isEmpty() && !hora.isEmpty()){
            ContentValues registro = new ContentValues();
            registro.put(DBManager.ID, codigo);
            registro.put(DBManager.PLACA, placa );
            registro.put(DBManager.PLAZA, plaza);
            registro.put(DBManager.FECHA, fecha);
            registro.put(DBManager.HORA, hora);

            db.insert(DBManager.TABLE_NAME, null, registro);

            db.close();

            editTextCodigo.setText("");
            editTextPlaca.setText("");
            editTextPlaza.setText("");
            editTextFecha.setText("");
            editTextHora.setText("");

            Toast.makeText(this, "Ingreso de datos correcto", Toast.LENGTH_LONG).show();
            finish();
        }else{

            Toast.makeText(this, "Debe llenar todos los campos", Toast.LENGTH_LONG).show();
        }


    }
}
