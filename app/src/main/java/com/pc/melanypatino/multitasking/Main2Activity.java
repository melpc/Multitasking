package com.pc.melanypatino.multitasking;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    EditText editTextConsultar;
    Button buttonConsultar, buttonModificar;
    TextView textViewPlaca, textViewPlaza, textViewFecha, textViewHora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        editTextConsultar = (EditText) findViewById(R.id.editTextConsultar);
        textViewPlaca = (TextView) findViewById(R.id.textViewPlaca);
        textViewPlaza = (TextView) findViewById(R.id.textViewPlaza);
        textViewFecha = (TextView) findViewById(R.id.textViewFecha);
        textViewHora = (TextView) findViewById(R.id.textViewHora);

        buttonConsultar = (Button) findViewById(R.id.buttonConsultar);
        buttonConsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Consultar(v);
            }
        });

        buttonModificar = (Button) findViewById(R.id.buttonModificar);
        buttonModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Modificar(v);
            }
        });

    }

    public void Consultar(View view){
        DBHelper admin = new DBHelper(this);
        SQLiteDatabase db = admin.getReadableDatabase();
        String codigo = editTextConsultar.getText().toString();
        if (!codigo.isEmpty()){
            Cursor fila = db.rawQuery
                    ("select * from "+DBManager.TABLE_NAME+" where codigo =" + codigo , null);
            if (fila.moveToFirst()){
                textViewPlaca.setText(fila.getString(1));
                textViewPlaza.setText(fila.getString(2));
                textViewFecha.setText(fila.getString(3));
                textViewHora.setText(fila.getString(4));

                db.close();

            }else{
                Toast.makeText(this, "El código ingresado no existe", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Main2Activity.this, IngresarActivity.class);
                db.close();
                startActivity(intent);
            }

        }
    }

    public void Modificar (View view){
        DBHelper admin = new DBHelper(this);
        SQLiteDatabase db = admin.getReadableDatabase();
        String codigo = editTextConsultar.getText().toString();
        if (!codigo.isEmpty()){
            Intent intent = new Intent(Main2Activity.this, ModificarActivity.class);
            db.close();
            startActivity(intent);
        }else{
            Toast.makeText(this, "Ingrese codigo a modificar", Toast.LENGTH_LONG).show();

        }

    }
}
